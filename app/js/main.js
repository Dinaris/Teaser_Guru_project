
TweenMax.fromTo('.header', 0.7, {y:-100}, {y: 0});
TweenMax.fromTo('.banner_laptop', 0.5, {scale: 0, delay: 1, opacity: 0}, {scale: 1, opacity: 1, delay: 1});

var parallax = document.querySelectorAll('.banner_parallax');
TweenMax.staggerFromTo (parallax, 1, 
    {opacity: 0, scale: 0.5, delay: 1.5}, 
    {opacity:1, scale: 1, delay: 1.5}, 
    0.2);
var cont = document.querySelectorAll('.content_btn');
TweenMax.staggerFrom (cont, 1,
    {opacity: 0, scale: 0.5, delay: 2.5, y: 100},
0.5);
var tl = new TimelineMax({ 
    repeat: -1,
    repeatDelay: 3 //delay the repeat by 0.05 seconds so that it's a total of 1 second long (last stagger is delayed 0.45 seconds, and the tween is 0.5 seconds long = 0.95 seconds total)
  }); 
  
  tl.to(
    '.scroll-button',   //target (all span tags)
     2,     //duration (0.5 seconds)
     {y:50},   //"from" values,  //"to" values
     -1   //stagger amount (seconds between each start time)
  );

//=====parallax=====//
//var scene = document.getElementById('scene');
//var parallaxInstance = new Parallax(scene);

    